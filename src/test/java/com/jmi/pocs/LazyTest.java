package com.jmi.pocs;

import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

/*
   The point is:

      1. We assign a lambda to the heavy private Supplier attribute.
      2. From there we call the syncronized method createAndCacheHeavy.
      3. createAndCacheHeavy commutes the lambda by an instance that implements the Supplier interface.
         This instance has no syncronized method.
      4. Subsequent calls need not pay the syncronized toll.

   The sample is borrowed from the book:

      Venkat Subramanian. Functional Programming in Java. The Pragmatic Bookshelf, 2014.

*/

class Heavy {
}

class Holder {
   private Supplier<Heavy> heavy = () -> createAndCacheHeavy();

   public Heavy getHeavy() {
      return heavy.get();
   }

   private synchronized Heavy createAndCacheHeavy() {
      class HeavyFactory implements Supplier<Heavy> {
         private final Heavy heavyInstance = new Heavy();

         @Override
         public Heavy get() {
            System.out.println("Reusing heavy...");
            return this.heavyInstance;
         }
      }

      if (!HeavyFactory.class.isInstance(heavy)) {
         heavy = new HeavyFactory();
      }

      System.out.println("Creating heavy...");

      return heavy.get();
   }
}

class LazyTest {

   @Test
   void testLazyObjectCreation() {
      Holder holder = new Holder();
      holder.getHeavy();
      holder.getHeavy();
   }
}
