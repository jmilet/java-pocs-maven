package com.jmi.pocs;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

class ExceptionTestHelper {
   public static <X extends Throwable> Throwable assertThrows(final Class<X> exceptionClass, final Runnable block) {
      try {
         block.run();
      } catch (Throwable ex) {
         if (exceptionClass.isInstance(ex)) {
            return ex;
         }
      }

      fail("Failed to throw expected exception");
      return null;
   }
}

class ArtificialException extends RuntimeException {
   private static final long serialVersionUID = 1L;
}

class ExceptionTest {

   static void operationThatThrowsArtificialException() {
      throw new ArtificialException();
   }

   @Test
   void testException() {
      ExceptionTestHelper.assertThrows(ArtificialException.class, () -> operationThatThrowsArtificialException());
   }
}
