package com.jmi.pocs;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

class RegisterBuilder {
   private RegisterBuilder() {}

   private int id;
   private String name;

   public RegisterBuilder setId(int id) {
      this.id = id;
      return this;
   }

   public int getId() {
      return this.id;
   }

   public String getName() {
      return this.name;
   }

   public RegisterBuilder setName(String name) {
      this.name = name;
      return this;
   }

   public static String build(Consumer<RegisterBuilder> builder) {
      RegisterBuilder instance = new RegisterBuilder();

      builder.accept(instance);

      return String.format("%d -> %s", instance.id, instance.name);
   }
}

class LoanPatternTest {
   
   @Test
   void testLoarPattern() {
      final int id = 100;
      final String name = "the name";

      final String result = RegisterBuilder.build(builder -> builder.setId(id)
                                                                    .setName(name));

      assertEquals("100 -> the name", result);
   }
}
