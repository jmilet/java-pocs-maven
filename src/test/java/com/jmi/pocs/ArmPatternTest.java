package com.jmi.pocs;

import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

@FunctionalInterface
interface UseInstance<T, X extends Throwable> {
   void accept(T instance) throws X;
}

class ArmResource {
   private ArmResource() {
   }

   public static void use(Path path, final UseInstance<FileWriter, Exception> block) throws Exception {
      FileWriter file = new FileWriter(path.toString());

      try {
         block.accept(file);
      } finally {
         file.close();
      }
   }
}

class ArmPatternTest {

   private static final String LAMBDA_RULES = "lambda|rules";

   @Test
   void testArmPattern() {
      final Path path = Paths.get("/tmp/ArmResource.txt");

      try {

         ArmResource.use(path, file -> {
            file.write("lambda\n");
            file.write("rules\n");
         });

         final String result = Files.readAllLines(path).stream().collect(joining("\n")).replace('\n', '|');

         final String expected = LAMBDA_RULES;

         assertEquals(expected, result);

         Files.deleteIfExists(path);

      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}
