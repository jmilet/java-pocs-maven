package com.jmi.pocs;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class Data {
   public static class Builder {
      private int id;
      private String name;

      private Builder() {}
      
      public Builder setId(int id) {
         this.id = id;
         return this;
      }
      
      public Builder setName(String name) {
         this.name = name;
         return this;
      }
      
      public Data build() {
         return new Data(id, name);
      }
   }
   
   private int id;
   private String name;
   
   public Data(int id, String name) {
      this.id = id;
      this.name = name;
   }
   
   public static Builder builder() {
      return new Builder();
   }
   
   public int getId() {
      return this.id;
   }
   
   public String getName() {
      return this.name;
   }
}


class BuilderTest {

   @Test
   void testBuilder() {
      final int theId = 10;
      final String theName = "Modern Java rocks";

      final Data data = Data.builder()
                            .setId(theId)
                            .setName(theName)
                            .build();
      
      assertEquals(theId, data.getId());
      assertEquals(theName, data.getName());
   }
}